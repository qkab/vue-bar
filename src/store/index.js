import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const state = {
  products: [
    {
      id: 1, title: 'Desperados', category: 'beer', price: 5,
    },
    {
      id: 2, title: 'Skoll', category: 'beer', price: 6,
    },
    {
      id: 3, title: 'Water', category: 'soft', price: 1,
    },
    {
      id: 4, title: 'Coca-cola', category: 'soft', price: 3,
    },
    {
      id: 5, title: 'Milk', category: 'soft', price: 2,
    },
    {
      id: 6, title: 'Hamburger', category: 'burger', price: 5,
    },
    {
      id: 7, title: 'Cheeseburger', category: 'burger', price: 6,
    },
    {
      id: 8, title: 'Fries', category: 'side', price: 3,
    },
    {
      id: 9, title: 'Potatoes', category: 'side', price: 3,
    },
    {
      id: 10, title: 'Bread', category: 'side', price: 2,
    },
  ],
  order: [],
  oldOrders: [],
};
const getters = {
  order: ({ order }) => order,
  oldOrders: ({ oldOrders }) => oldOrders,
  allProducts: ({ products }) => products,
};
const mutations = {
  pushToOrder: ({ order }, product) => order.push(product),
  processOrder: ({ oldOrders, order }, menu) => {
    oldOrders.push(menu);
    order.splice(0, order.length);
  },
};
const actions = {
  pushProduct: ({ commit }, product) => {
    commit('pushToOrder', product);
  },
  orderMenu: ({ commit }, order) => {
    const price = order.reduce((result, item) => result + item.price, 0);
    const index = state.oldOrders.length + 1;
    commit('processOrder', {
      id: index, title: `order-${index}`, menu: order, price, status: 'passed',
    });
    // eslint-disable-next-line no-alert
    setTimeout(() => alert(`Order successfully payed ${price}€`), 1000);
  },
};
const menuModule = {
  state,
  mutations,
  actions,
  getters,
};
export default new Vuex.Store({
  modules: { menuModule },
});
